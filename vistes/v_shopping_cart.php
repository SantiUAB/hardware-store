<?php

$cantTotal = 0;
$priceTotal = 0;
if(isset($_SESSION['cantTotal'])){
    $cantTotal = $_SESSION['cantTotal'] ?? 0;
}

if(isset($_SESSION['priceTotal'])){
    $priceTotal = $_SESSION['priceTotal'] ?? 0;
}

if(isset($addCorrect)){

    if($addCorrect){
        echo "<script type='text/javascript'>alert('Producto añadido correctamente.');</script>";
    }else{
        echo "<script type='text/javascript'>alert('Error al añadir producto.');</script>";
    }
}
?>

<div id="cart-container">
    <a href="/index.php?action=cart_detail"><button><h1>Ver Cesta</h1></button></a>

    <h2 id="cantTotal">Cantidad productos : <?php echo  $cantTotal; ?></h2>
    <h2 >Precio total <?php echo  $priceTotal ; ?> €</h2>
</div>


