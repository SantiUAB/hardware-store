<head>
    <meta charset="utf-8"/>
    <link rel="stylesheet" href="/../../css/web.css">

</head>

<?php $filesPublicPath = '/fitxers/'; ?>
<body>
    <form action="/../index.php?action=account" method="post" enctype="multipart/form-data">
        <label for="profile_image" />
            <img
                    src="<?php echo $filesPublicPath.$user['imageName'] ?>"
                    alt="<?php echo $user['name']?>"
                    width="200px"
            />
        </label>
        <input type="file" name="profile_image" id="profile_image""/>

        <label for="name" />Nom i Cognoms</label>
        <input type="text" name="name" id="name" value="<?php echo $user['name']?>"/>

        <label for="address" />Adreça</label>
        <input type="text" name="address" id="address" value="<?php echo $user['address']?>"/>

        <label for="poblation" />Població</label>
        <input type="text" name="poblation" id="poblation" value="<?php echo $user['poblation']?>"/>

        <label for="postal_code" />Codi Postal</label>
        <input type="text" name="postal_code" id="postal_code" value="<?php echo $user['postal_code']?>" pattern="[0-9]{5}"/>

        <input type="submit" name="submit" value="Actualitzar Informació" />
    </form>
</body>