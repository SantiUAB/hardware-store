
<head>
    <h1>Productes trobats amb la cerca: "<?php echo $inputText ?>" </h1>
</head>
<div id="container-products">

    <?php foreach($products as $product) { ?>

        <a id="<?php echo $product['id'] ?>"  >
            <div id="img-product">
                <h3><?php echo $product['name']; ?> </h3>
                <img src=<?php echo $product['img'] ?>>

            </div>
        </a>

    <?php } ?>
</div>
<script type="text/javascript">

    $(document).ready(function(){


        $("#container-products a").click(function(){
            var id = $(this).attr('id');
            // console.log(id );
            $.ajax({
                url: "/../index.php?action=product_detail&product="+ id, success:
                    function(result){
                        $("#center-wrapper").html(result);
                    }

            });
            //alert("ID PRODUCTO " + id );
        });
    })
</script>

</head>

