<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <link rel="stylesheet" type ="text/css" href="css/web.css" />
    <script src="js/funcions.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
</head>

<body>

<header>
    <?php include __DIR__."../controladors/header.php"?>

</header>
<!--main content-->
<section class="main-content">

    <!--center-wrapper-->
    <div class="center-wrapper">
        <?php include __DIR__."/controladors/products.php"?>
    </div>
    <!--end center-wrapper-->
</section>
<!--end main content-->



</body>
</html>