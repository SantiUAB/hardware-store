

<div id="cart-detail-container">

    <?php

    $cantTotal = 0;
    $priceTotal = 0;
    if(isset($_SESSION['cantTotal'])){
        $cantTotal = $_SESSION['cantTotal'] ?? 0;
    }

    if(isset($_SESSION['priceTotal'])){
        $priceTotal = $_SESSION['priceTotal'] ?? 0;
    }

    if(isset($message)){
        echo "<script type='text/javascript'>alert($message);</script>";
    }
    ?>

    <!-- <?php /*echo $message */?> -->
    <h1>Detalle de compra</h1>
    <a href="/index.php"><BUTTON>Inico</BUTTON></a>

    <div class="detail-container">

        <?php
        //echo $message; // msg INFO
        if(isset($_SESSION['card'])){

            if(count($_SESSION['card']) ==0){
                ?> <H2> Cesta esta vacia </H2> <?php
            }else{
                ?>
                <a href="/index.php?action=checkout"><button>Tramitar pedido</button></a>
                <h2>Cantidad productos : <?php echo  $cantTotal; ?></h2>
                <h2>Precio total <?php echo  $priceTotal ; ?> €</h2>
                <h2>Tu cesta </h2>
                <button id="cart-empty" >Vaciar cesta</button> <br>
                <?php
                foreach ($_SESSION['card'] as &$listProduct){ ?>

                    <?php foreach ($listProduct as &$product){?>

                        <div class="list-detail-products" id="<?php echo $product['id']; ?>">
                            <h3 >Nombre producto : <?php echo $product['name']; ?> </h3>
                            <h3>Precio Unidad: <?php echo $product['price']; ?> €</h3>
                            <button id="bttAdd" value="<?php echo $product['id']; ?>">+</button>
                            <h3 id="cant"><?php echo $product['cant']; ?> unidades</h3>
                            <button id="bttSub" value="<?php echo $product['id']; ?>">-</button> <br>
                            <button id="del" value="<?php echo $product['id']; ?>">Eliminar</button> <br>

                            <img src=<?php echo $product['img'] ?>>

                        </div>
                        <?php
                    }
                }
                ?> <a href="/index.php?action=checkout"><button>Tramitar pedido</button></a>
                <?php
            }
        }

       ?>


    </div>
</div>

<script type="text/javascript">
    $(document).ready(function (){
        $(document).on('click', '#bttAdd',function (){
            var id = $(this).attr('value');
            var strURL = "/../index.php?action=cart_detail&func=add&id_cant=" + id;
            $.ajax({

                url: strURL,
                success:
                    function(result){

                        $('#cart-detail-container').html(result);
                    }

            });
        });

        $(document).on('click', '#bttSub',function (){
            var id = $(this).attr('value');

            var  srtURL= "./../index.php?action=cart_detail&func=sub&id_cant=" + id;

            $.ajax({
                //type: 'POST',
                url: srtURL,
                success:
                    function(result){

                        $('#cart-detail-container').html(result);
                    }

            });
        });

        $(document).on('click', '#del',function (){
            var id = $(this).attr('value');
            var  srtURL= "./../index.php?action=cart_detail&func=del&id_cant=" + id;
            $.ajax({
                url: srtURL,
                success:
                    function(result){

                        $('#cart-detail-container').html(result);
                    }

            });
        });

        $(document).on('click', '#cart-empty',function (){
            var  srtURL= "./../index.php?action=cart_detail&func=empty";

            $.ajax({

                url: srtURL,
                success:
                    function(result){

                        $('#cart-detail-container').html(result);
                    }

            });
        });

    });

</script>