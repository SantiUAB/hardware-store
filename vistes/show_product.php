
<div id="container-one-product" >
    <h1 id="product-title"><?php echo $product['name']; ?> </h1>

    <div id="img-product" >
        <img src=<?php echo $product['img'] ?>>
    </div>

    <div id="detail-product">
        <h3> Preu: <?php echo $product['price'] ?> </h3>
        <p><?php echo $product['description'] ?></p>
        <button id="btt-buy" value="<?php echo $product['id'] ?>">Comprar</button>
    </div>
</div>

<script type="text/javascript">

    $(document).ready(function (){

        $("#btt-buy").click(function (){
            var id = $(this).attr('value');
            //alert("ID PRODUCTO " + id );
            console.log("ID PRODUCTO " + id );
            $.ajax({
                url: "./../index.php?action=buy_product&id_buy=" + id,
                success:
                    function(result){
                        //alert('id_buy =' + id);
                        $("#cart-container").html(result);
                    }

            });

        });
    })
</script>
