<html lang="es">


    <header>
        <?php require_once __DIR__.'/controladors/header.php'?>
    </header>

<section class="main-content">

    <!--center-wrapper-->
    <div id="center-wrapper">
        <?php include_once __DIR__."/controladors/products.php"?>
        <?php include_once __DIR__."/controladors/c_shopping_card.php"?>

    </div>
    <!--end center-wrapper-->
</section>

</html>