<html lang="es">
<head>
    <meta charset="utf-8"/>
    <link rel="stylesheet" type ="text/css" href="css/web.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
</head>

<?php
session_start();
//print_r($_SESSION);
$action = $_GET['action'] ?? null;
$filesAbsolutePath = '/home/TDIW/tdiw-e11/public_html/fitxers/';

switch ($action){
    case 'productes':
        require_once __DIR__ . '/recourse_products.php';
        break;
    case 'product_detail':
        require_once __DIR__ . '/recourse_product_detail.php';
        break;
    case 'login':
        require_once __DIR__.'/recourse_login.php';
        break;
    case 'login_drop_down':
        require_once __DIR__.'/recourse_login_drop_down.php';
        break;
    case 'logout':
        require_once __DIR__.'/recourse_logout.php';
        break;
    case 'register':
        require_once __DIR__.'/recourse_register.php';
        break;
    case 'register_existent':
        require_once __DIR__.'/recourse_register_existent.php';
        break;
    case 'register_correcte':
        require_once __DIR__.'/recourse_register_correcte.php';
        break;
    case 'searchProducts':
        require_once __DIR__.'/recourse_searchProducts.php';
        break;
    case 'buy_product':
        require_once __DIR__ . '/recourse_shopping_cart.php';
        break;
    case 'cart_detail':
        require_once __DIR__.'/r_shopping_cart_detail.php';
        break;
    case 'checkout':
        require_once __DIR__.'/recourse_checkout.php';
        break;
    case 'account':
        require_once __DIR__.'/recourse_account.php';
        break;
    case 'myShopping':
        require_once __DIR__.'/recourse_shopping.php';
        break;
    default:
        require __DIR__.'/recourse_default.php';
        break;

}
//print_r($_SESSION);
?>
</html>