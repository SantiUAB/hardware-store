<?php
    $message = 'sin ejecuciones';
    function checkout($connection){
    $cart_id = -1;
    try{

        $id_user = intval($_SESSION['user_id']);
        $priceTotal = floatval($_SESSION['priceTotal']);
        $cantTotal = intval($_SESSION['cantTotal']);
        //$sentence = 'INSERT INTO Cart( user_id, time_stamp,total_units) VALUES ( :id_user, CURRENT_TIMESTAMP , :priceTotal, :cantTotal)';
        // INSERT Funcional
        $sentence = "INSERT INTO Cart( user_id, time_stamp,total_price,total_units) VALUES ( $id_user, CURRENT_TIMESTAMP , $priceTotal, $cantTotal)";
        $query = $connection->prepare($sentence);
        $queryOk = $query->execute();

        /* Intento 1 de bind

        $sentence = 'INSERT INTO Cart( user_id, time_stamp,total_units) VALUES ( :id_user, CURRENT_TIMESTAMP , :priceTotal, :cantTotal)';
        $query = $connection->prepare($sentence);
        $queryOk = $query->execute(
            [
                'id_user'=> $id_user,
                'priceTotal' => $priceTotal,
                'cantTotal' => $cantTotal
            ]
        );*/

        /* Intento 2 de bind
        $sentence = 'INSERT INTO Cart( user_id, time_stamp,total_units) VALUES ( :id_user, CURRENT_TIMESTAMP , :priceTotal, :cantTotal)';
        $query = $connection->prepare($sentence);

        $query->bindParam(':id_user', $id_user, PDO::PARAM_STR);
        $query->bindParam(':priceTotal', $priceTotal, PDO::PARAM_STR); // No existe el bind float
        $query->bindParam(':cantTotal', $cantTotal, PDO::PARAM_STR); // No existe el bind float

        $queryOk = $query->execute();
        */

       /* // Intento 3 de bind
        $sentence = 'INSERT INTO Cart( user_id, time_stamp,total_units) VALUES ( :id_user, CURRENT_TIMESTAMP , :priceTotal, :cantTotal)';
        $query = $connection->prepare($sentence);
        $query->bindValue('id_user', $id_user);
        $query->bindValue('priceTotal', $priceTotal);
        $query->bindValue('cantTotal', $cantTotal);
        $queryOk = $query->execute();
       */

        /* Intento 4 de bind
        $sentence = 'INSERT INTO Cart( user_id, time_stamp,total_units) VALUES ( ?, CURRENT_TIMESTAMP , ?, ?)';

        $query = $connection->prepare($sentence);

        $query ->bind_param('ssd', $id_user, $priceTotal, $cantTotal);
        $queryOk = $query->execute();
        */

        $cart_id = $connection->lastInsertId(); //Obtener ultimo Cart insertado.

        if($queryOk){
            //echo "<script type='text/javascript'>alert('ID ultimo insert' + $connection-> insert_id) ;</script>";
            foreach ($_SESSION['card'] as $listProduct){
                foreach ($listProduct as $product){

                    $id_product = intval($product['id']);
                    $cant = intval($product['cant']);
                    $name_product = strval($product['name']);
                    $price = $product['price'];
                    //INSERT INTO `line_cart` (`product_id`, `card_id`, `quantity`, `name`, `price`) VALUES ('1', '105', '5', 'affas', '1');
                    $sentence = 'INSERT INTO line_cart(product_id, card_id, quantity, name, price) VALUES (:user_id, :card_id, :cant, :name_product, :price )';

                    $query = connectaBD()->prepare($sentence);

                    $query->execute(
                    [
                        'user_id' =>$id_product,
                        'card_id' =>$cart_id,
                        'cant' =>$cant,
                        'name_product' =>$name_product,
                        'price' =>$price

                    ]
                    );
                }
            }

            $_SESSION['card'] = array();
            $_SESSION['cantTotal'] = 0;
            $_SESSION['priceTotal'] = 0;

            return $cart_id; //ejecucion correcta
        }
        $cart_id = -1;
        return $cart_id; //error en el insert

    }catch (PDOException $e){
        //echo "PDOException".$e->getMessage(); // Mensaje de error, no comprensible para el usuario
        $cart_id = -1;
        return $cart_id;
    }catch (Exception $e){
        //echo "Exception".$e->getMessage(); // Mensaje de error, no comprensible para el usuario
        $cart_id = -1;
        return $cart_id;
    }

}

    function sendCheckout(){

        $res = getMail();
        if($res !='errorGetMail'){
            $listProducts = getCartProducts();
            $_SESSION['user_mail']= $res;
            $_SESSION['title_mail'] = 'Detalle de compra';
            $str = 'Ha comprado los siguientes productos:\n'.$listProducts;
            $_SESSION['message_mail'] = wordwrap($str, strlen($str), '\n');
            $_SESSION['last_cart'] = $_SESSION['card'];

            /*// ver si funciona email? // tras hablaro con Ana, parece que esta capado
             * if(mail($_SESSION['user_mail'], $_SESSION['title_mail'] , $_SESSION['message_mail'])){
                echo "<script type='text/javascript'>alert('Resumen de compra enviado a ') ;</script>";
            }else{
                echo "<script type='text/javascript'>alert('Error al enviar resumen de compra enviado a ') ;</script>";
            }*/
        }
        return $res;
    }

    /*Funcion que crea el mensaje para el e-mail.
    Return devuelve el listado de productos comprados*/
    function getCartProducts(){

        $msg = 'no hay productos en la cesta';
        $listProductsCart ='';
        if(count($_SESSION['card'])==0){
            return $msg;
        }

        foreach ($_SESSION['card'] as $listProduct){

            foreach ($listProduct as $product){

                $listProductsCart = '- '.$listProductsCart.$product['name'].'\n\tcoste unidad: '.$product['price'].'€.\n\t Unidades compradas:'.$product['cant'].'get\n';
            }
        }
        return $listProductsCart;
    }
?>