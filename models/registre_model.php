<?php

function checkRegister($connection) {
    try {
        $query = $connection->prepare("SELECT COUNT(*) as c FROM user WHERE email = :email");
        $query->bindParam(':email', $_POST['email'], PDO::PARAM_STR);
        $query->execute();
        $result = $query->fetch(PDO::FETCH_ASSOC);
        if ($result['c'] == 1) {
            return true;
        }
        else {
            return false;
        }
    } catch(PDOException $e) {
        echo "Error".$e->getMessage();
    }
}

function register($connection) {
    $sentence = 'INSERT INTO user(name, email, password, address, poblation, postal_code) VALUES (:name, :email, :password, :address, :poblation, :postal_code)';
    $query = $connection->prepare($sentence);

    $passwordHash = password_hash($_POST['password'], PASSWORD_DEFAULT);

    $query->bindParam(':name', $_POST['nombre'], PDO::PARAM_STR);
    $query->bindParam(':email', $_POST['email'], PDO::PARAM_STR);
    $query->bindParam(':password', $passwordHash, PDO::PARAM_STR);
    $query->bindParam(':address', $_POST['address'], PDO::PARAM_STR);
    $query->bindParam(':poblation', $_POST['city'], PDO::PARAM_STR);
    $query->bindParam(':postal_code', $_POST['codigo-postal'], PDO::PARAM_STR);

    $query->execute();
}

?>