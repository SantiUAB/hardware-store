<?php
    function getProductsCategory($connection, $id){
        $products = '';
        try{


            $query = $connection->prepare("SELECT id, category_id,name, price, description, img FROM Product WHERE category_id= :id");
            $query->bindParam(':id', $id, PDO::PARAM_STR);
            $query->execute();
            $products = $query->fetchAll(PDO::FETCH_ASSOC);

            return $products;

        }catch(PDOException $e){
            echo "Error: " . $e->getMessage();
        }

        return $products;
    }

    function getProductsDefault($connection){
        try{


            $query = $connection->prepare("SELECT id, category_id,name, price, description, img FROM Product");

            $query->execute();
            $categories = $query->fetchAll(PDO::FETCH_ASSOC);

            return $categories;

        }catch(PDOException $e){
            echo "Error: " . $e->getMessage();
        }

        return $categories;
    }


function getProduct($connection, $id){
    try{

        $query = $connection->prepare("SELECT id, category_id,name, price, description, img FROM Product WHERE id= :id");
        $query->bindParam(':id', $id, PDO::PARAM_STR);
        $query->execute();
        $product = $query->fetch(PDO::FETCH_ASSOC);

        //echo "ID =".$id;
        //print_r(array_values($categories));
        //var_dump($product );
        return $product ;

    }catch(PDOException $e){
        echo "Error: " . $e->getMessage();
    }

    return $product;
}

