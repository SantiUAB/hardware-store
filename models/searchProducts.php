<?php
function searchProducts($inputText, $connection)
{
    $sqlQuery = $connection->prepare('SELECT id, name, description, img, price FROM Product where name LIKE :text');
    $sqlQuery->bindValue('text', "%".$inputText."%");
    $sqlQuery->execute();

    $query = $sqlQuery->fetchAll(PDO::FETCH_ASSOC);

    return $query;
}
