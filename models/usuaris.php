<?php
function login(string $email, string $password)
{
    $sql = 'SELECT id, email, password 
              FROM user
              where email = :email 
              LIMIT 1';

    $stmt = connectaBD()->prepare($sql);
    $stmt->execute(
        [
            'email' => $email,
        ]
    );


    $result = $stmt->fetch(PDO::FETCH_ASSOC);
    echo '<script>console.log("log result \$value\")</script>';
    if($result ===false){
        return null;
    }

    return password_verify($password, $result['password']) ? $result : null;
}

function getMail(){

    $userMail = 'errorGetMail';
    try{

        $sentence = 'SELECT email 
              FROM user
              where id = :user_id 
              LIMIT 1';
        $query = connectaBD()->prepare($sentence);

        $query->execute(
            [
                'user_id' => intval($_SESSION['user_id']),
            ]
        );
        $userMail = $query->fetch(PDO::FETCH_ASSOC);
        $_SESSION['email'] = $userMail['email'];
        return $userMail['email'];

    }catch (PDOException $e){
        echo "PDOException ".$e;
        return $userMail;
    }catch (Exception $e){
        echo "Exception ".$e;
        return $userMail;
    }
}

function getUserInfo($userId){
    try{
        $sentence = 'SELECT name, address, poblation, postal_code, imageName FROM user where id = :user_id';
        $query = connectaBD()->prepare($sentence);
        $query->execute(['user_id' => $userId]);
        $result = $query->fetch(PDO::FETCH_ASSOC);
    }catch(PDOException $e){
        echo "Error: " . $e->getMessage();
    }
    return $result;
}

function updateUser($userId){
    try{
        $filesAbsolutePath = '/home/TDIW/tdiw-e11/public_html/fitxers/';
        $sentence = 'UPDATE user SET name = :name, address = :address, 
                        poblation = :poblation, postal_code = :postal_code, imageName = :imageName
                            WHERE id= :userId';
        $query = connectaBD()->prepare($sentence);
        $query->bindParam(":name", $_POST['name'], PDO::PARAM_STR);
        $query->bindParam(":address", $_POST['address'], PDO::PARAM_STR);
        $query->bindParam(":poblation", $_POST['poblation'], PDO::PARAM_STR);
        $query->bindParam(":postal_code", $_POST['postal_code'], PDO::PARAM_STR);
        $query->bindParam(":imageName", $userId, PDO::PARAM_STR);
        $query->bindParam(":userId", $userId, PDO::PARAM_STR);
        $query->execute();


        if(isset($_FILES['profile_image']) && !empty($_FILES['profile_image'])) {
            $destinationPath = $filesAbsolutePath.$userId;
            move_uploaded_file($_FILES['profile_image']['tmp_name'], $destinationPath);
        }
    }catch(PDOException $e){
        echo "Error: " . $e->getMessage();
    }
}

function getCarts($userId){
    try{
        $sentence = 'SELECT * FROM Cart where user_id = :user_id';
        $query = connectaBD()->prepare($sentence);
        $query->execute(['user_id' => $userId]);
        $result = $query->fetchAll(PDO::FETCH_ASSOC);
    }catch(PDOException $e){
        echo "Error: " . $e->getMessage();
    }
    return $result;
}

function getLineCarts(){
    try{
        $sentence = 'SELECT * FROM line_cart';
        $query = connectaBD()->prepare($sentence);
        $query->execute();
        $result = $query->fetchAll(PDO::FETCH_ASSOC);
    }catch(PDOException $e){
        echo "Error: " . $e->getMessage();
    }
    return $result;
}

function getProducts(){
    try{
        $sentence = 'SELECT id, img FROM Product';
        $query = connectaBD()->prepare($sentence);
        $query->execute();
        $result = $query->fetchAll(PDO::FETCH_ASSOC);
    }catch(PDOException $e){
        echo "Error: " . $e->getMessage();
    }
    return $result;

}
?>