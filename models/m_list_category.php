<?php
    function getCategory($connection){
        try{
            $consulta = $connection->prepare("SELECT name, id FROM Category");
            $consulta->execute();
            $categories = $consulta->fetchAll(PDO::FETCH_ASSOC);
        }catch(PDOException $e){
            echo "Error: " . $e->getMessage();
        }
        return $categories;
    }
?>