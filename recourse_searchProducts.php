<html lang="es">
<head>
    <link rel="stylesheet" href="css/web.css">
</head>
<header>
    <?php require_once __DIR__.'/controladors/header.php'?>
</header>
<section class="main-content">

    <!--center-wrapper-->
    <div id="center-wrapper">
        <?php require_once __DIR__.'/controladors/searchProducts.php'?>
        <?php include_once __DIR__."/controladors/c_shopping_card.php"?>
    </div>
    <!--end center-wrapper-->
</section>
</html>
