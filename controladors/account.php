<?php
include_once __DIR__ . "/../models/m_connect-db.php";
require_once __DIR__.'/../models/usuaris.php';

$filesAbsolutePath = '/home/TDIW/tdiw-e11/public_html/fitxers/';
$userId = $_SESSION['user_id'];
$user = getUserInfo($userId);

if(isset($_POST['submit'])) {
    updateUser($userId);
    header('Location: index.php?action=account');
}
require_once __DIR__.'/../vistes/edit_profile.php';

