<?php
include_once __DIR__ . "/../models/m_connect-db.php";
require_once __DIR__.'/../models/usuaris.php';

$userId = $_SESSION['user_id'];
$cart = getCarts($userId);
$lineCart = getLineCarts();
$products = getProducts();

require_once __DIR__.'/../vistes/shopping.php';