<?php
require_once __DIR__ . '/../models/m_connect-db.php';
require_once __DIR__.'/../models/searchProducts.php';

$connection = connectaBD();
$inputText = $_GET['search'] ?? null;
$products = searchProducts($inputText, $connection);

if($products != null){
    require_once __DIR__.'/../vistes/searchProducts.php';
}
else{
    require_once __DIR__.'/../vistes/productNotFound.php';

}