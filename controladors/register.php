<?php

    if(isset($_POST['enviar'])) {
        include_once __DIR__ . "/../models/m_connect-db.php";
        include_once __DIR__ . "/../models/registre_model.php";
        $connection = connectaBD();

        $email = $_POST['email'];
        $isEmail = filter_var($email, FILTER_VALIDATE_EMAIL);
        if ($isEmail == true) {
            $found = checkRegister($connection);
            if (!$found) {
                register($connection);
                header("location:/../index.php?action=register_correcte");
            } else {
                //echo' <script type="text/javascript"> alert("correo ya existe!"); </script>';

                include_once __DIR__ . "/../vistes/register_existent.php";

            }
        } else {
            echo' <script type="text/javascript"> alert("Format del email incorrecte!"); window.location.href="/../index.php?action=register"; </script>';
        }
    }else {
            include_once __DIR__ . "/../vistes/register_vista.php";
    }


?>